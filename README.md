# My Website
<div align="center">
<img src="Banner_README.png" />
</div>

## Table of contents

- [General Info](#general-info)
- [Purpose](#purpose)
- [Website Structure](#website-structure)
- [Technologies](#technologies)
- [Technical Aspects](#technical-aspects)
- [Sources](#sources)
- [other information](#other-information)

## General Info

This website will display my portfolio to my future employers, and my blog where I write about what I love:

- development (I will build you an awesome website 🏆 ),
- security,
- RGPD,
- Blender,
- How to become a freelance,
- Chess.

## Website Structure

My website needs to be beautiful, clean and well organized. If my portfolio is clean, you will think my code is too, which for sure is... To make it fast and clean I will use **a template**.

### Sections

- Firstly comes the Hero section with a CTA "See project", and a direction to scroll.
- 2nd section: list of Projects and Clients, with accessible demo. A CTA contact me is accessible.
- 4th section: Coding challenges: Make it look cool fancy !
- 3rd section: About me/What we do: name, photo and mission statement. Education, skills and language.
- 5th section: technical writing.
- 6th section: Contact me.

Maybe the Hero section and the abou me section should merge.

## 🚀 Technologies

1. Gatsby

<p align="center">
  <a href="https://www.gatsbyjs.com">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>

Why did I choose Gatsby 🧐?

I know React, and want to learn more about it by discovering Gatsby. I build some website with vanilla html, css, and Javascript but also Jekyll. Thanks to Jekyll I discover what is a static websites generator, which is pretty cool! Gatsby allows me to discover something new and increase my knowledge of static websites generator.

## Roadmap

1. [x] Choose the template. 18.03.2021
1. [x] Homepage/HeroSection is implemented. 19.03.2021
1. [x] Homepage/ContactSection is implemented. 21.03.2021 -> 25.03.2021
1. [x] Homepage/ProjectSection is implemented. 20.03.2021
1. [x] Homepage/AboutmeSection is implemented. 22.03.2021
1. [x] Project Page is implemented. 23.03.2021 -> I don't need it.
1. [x] Homepage/Footer is implemented. 28.03.2021
1. [x] host a gatsby template to framagit. -> 30.03.2021
1. [x] host a gatsby template to framagit. -> 30.03.2021
1. [x] Letsencrypt certified! 31.03.2021
1. [ ] Add to the site the Homepage/HeroSection is implemented. 05.04.2021
1. [ ] Add to the site the Homepage/ContactSection is implemented. 10.04.2021
1. [ ] Add to the site the Homepage/ProjectSection is implemented. 15.04.2021
1. [ ] Add to the site theHomepage/AboutmeSection is implemented. 20.04.2021
1. [ ] Add to the site the Project Page is implemented. 25.04.2021
1. [ ] Add to the site the Homepage/Footer is implemented. 30.03.2021
1. [ ] Blog feature implemented. 24.03.2021 -> 31.03.2021
1. [ ] Blogging with Markdown. 25.03.2021 -> 05.04.2021
1. [ ] host a contactform api.
1. [ ] Homepage/WritingSection implemented. 26.03.2021 -> 05.04.2021
1. [ ] Implement React Helmet. 27.03.2021 -> 10.04.2021
1. [ ] Homepage/Fizzbuzzpage UI implemented. 30.03.2021 -> 15.04.2021
1. [ ] Homepage/Fizzbuzz Code implemented. 31.03.2021 -> 20.04.2021
1. [ ] Homepage/CodingChallengesSection is implemented. 01.04.2021 -> 25.04.2021

## Technical Aspects

### Scope of functionalities

1. portfolio presentation:
      - On the Homepage the 3 most recent projects are displayed and by clicking on a 4th button you can have access to a projects page, where all the project are shown.
1. blog
1. install a CMS
1. Manage SEO

### Project status

I need to implement one of this template:

- https://www.gatsbyjs.com/starters/codebushi/gatsby-starter-dimension
- https://www.gatsbyjs.com/starters/EmaSuriano/gatsby-starter-mate
- https://www.gatsbyjs.com/starters/cobidev/gatsby-simplefolio

### Project structure

A quick look at the top-level files and directories you'll see in a Gatsby project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1. **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2. **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

3. **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4. **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

5. **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.com/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.

6. **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.com/docs/gatsby-config/) for more detail).

7. **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.com/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

8. **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.com/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering.

9. **`LICENSE`**: This Gatsby starter is licensed under the 0BSD license. This means that you can see this file as a placeholder and replace it with your own license.

10. **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about your project.

### Installation process

- write in your terminal:

```bash
gatsby new my-gatsby-project https://framagit.org/qparrot/gatsby-portfolio.git
cd my-gatsby-project
gatsby develop
```

## Sources

### 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.com/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.com/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.com/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## Other Information

Author: Quentin Parrot
Contact: quentin.parrot@protonmail.com
Social media: framagit.com/qparrot
license: MIT
