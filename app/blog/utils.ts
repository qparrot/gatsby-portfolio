import fs from 'fs';
import path from 'path';


function parseFrontmatter(fileContent: string) {
  let frontmatterRegex = /---\s*([\s\S]*?)\s*---/
  let match = frontmatterRegex.exec(fileContent)
  let frontMatterBlock = match![1]
  let content = fileContent.replace(frontmatterRegex, '').trim()
  let frontMatterLines = frontMatterBlock.trim().split('\n')
  let metadata = {}

  frontMatterLines.forEach((line) => {
    let [key, ...valueArr] = line.split(': ')
    let value = valueArr.join(': ').trim()
    value = value.replace(/^['"](.*)['"]$/, '$1') // Remove quotes
    metadata[key.trim()] = value
  })
  return { metadata: metadata , content }
}


function getMDXFiles(dir) {
  const files = [];
  // Read the directory
  fs.readdirSync(dir).forEach(file => {
    const filePath = path.join(dir, file);
    // Check if the current path is a directory or a file
    if (fs.statSync(filePath).isDirectory()) {
      // If it's a directory, recursively get files
      files.push(...getMDXFiles(filePath));
    } else {
      // If it's a file, push the relative path
      files.push(path.relative(path.join(process.cwd(), "app", "blog", "posts"), filePath));
    }
  });
  return files;
}


function getMDXData(dir) {
  let mdxFiles = getMDXFiles(dir)
	return [{metadata: {title:"test"}, content: "hello", slug: "posts.mdx"}]
}

export function getBlogPosts() {
  return getMDXData(path.join(process.cwd(), 'app', 'blog', 'posts'))
}

export function formatDate(date: string, includeRelative = false) {
  let currentDate = new Date()
  if (!date.includes('T')) {
    date = `${date}T00:00:00`
  }
  let targetDate = new Date(date)

  let yearsAgo = currentDate.getFullYear() - targetDate.getFullYear()
  let monthsAgo = currentDate.getMonth() - targetDate.getMonth()
  let daysAgo = currentDate.getDate() - targetDate.getDate()

  let formattedDate = ''

  if (yearsAgo > 0) {
    formattedDate = `${yearsAgo}y ago`
  } else if (monthsAgo > 0) {
    formattedDate = `${monthsAgo}mo ago`
  } else if (daysAgo > 0) {
    formattedDate = `${daysAgo}d ago`
  } else {
    formattedDate = 'Today'
  }

  let fullDate = targetDate.toLocaleString('en-us', {
    month: 'long',
    day: 'numeric',
    year: 'numeric',
  })

  if (!includeRelative) {
    return fullDate
  }

  return `${fullDate} (${formattedDate})`
}
