import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Navbar from './components/navbar.js';
import React from 'react';
export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Quentin Parrot's Portfolio</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
			<Navbar />
      <main>
        <h1 className={styles.title}>
          Your Fullstack developer !
        </h1>


        <div className={styles.grid}>
          <a href="/blog" className={styles.card}>
            <h3>My first post</h3>
            <p>Learn about nextjs, gitlab-ci and DNS configuration !</p>
          </a>
        </div>
        <div className={styles.grid}>
          <a href="/blog/2023/2023-08-07" className={styles.card}>
            <h3>last post of 2023</h3>
          </a>
        </div>
      </main>

      <footer>
        <a
          href="https://framagit.org/qparrot"
          target="_blank"
          rel="noopener noreferrer"
        >
          Made by Quentin Parrot
        </a>
      </footer>

    </div>
  );
}
